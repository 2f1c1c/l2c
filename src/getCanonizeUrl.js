export default function getCanonizeUrl(url) {

	var markQuestionPosition = url.lastIndexOf('?');
	const lastSlashPosition = url.lastIndexOf('/');
	var string = url;

	const reg = /\.\w+\/\@?\w+\.?\w+\/\w+/i;
	
	if (reg.test(url)) {
		var firstSlash = url.indexOf('/', url.search(reg));
		var secondSlash = url.indexOf('/', firstSlash + 1);
		string = string.slice(firstSlash, secondSlash);
	}
	
	if (lastSlashPosition === url.length - 1) string = string.slice(0,-1);

	if (markQuestionPosition === -1) markQuestionPosition = url.length;

	var name = string.slice(string.lastIndexOf('/') + 1, markQuestionPosition);

	if (!/\@/.test(url)) name = '@' + name;
	console.log(url);
	console.log(name);
	return name;

}


