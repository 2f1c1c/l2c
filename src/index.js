import express from 'express';
import getCanonizeUrl from './getCanonizeUrl';

const app = express();

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.get('/', function(req,res) {
	
	const response = getCanonizeUrl(req.query.username);
	
	return res.send(response, {'Content-Type': 'text/plain'}, 200);
	

});

app.listen(3000, function() {
});


